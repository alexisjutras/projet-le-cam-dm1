### Lien de la page web problématique

Copier/Coller le lien ici

### Résumé du problème

Résumer précisement en quoi consiste de problème

### Impressions d'écran

Ajoutez des impressions d'écran permettant de clarifier ou de visualiser plus facilement le problème

### Correctifs possibles

Si vous pensez avoir une piste de solutions ou une suggestion pour diriger la correction de problème, écrivez le ici.

/label ~Interface
