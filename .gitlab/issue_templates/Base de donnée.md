### Problème avec la base de données

Écrivez ce qui c'est passé.

### Le problème

Décrire ce que le programme était supposé faire et, si possible, le message d'erreur.

### Étapes pour reproduire le problème

Incrire les étapes.

### Correctifs possibles

Inscrire les correctifs à faire.

/label ~"Base de donnée"
