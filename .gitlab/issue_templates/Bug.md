### Sommaire

Résumez le bug rencontré 

### Étapes à suivre pour reproduire le bug

Décrivez les étapes pour reproduire le problème. 

### Quel est le lien de la page rattaché au * bug *?

Écrivez le lien ici. 

### Quel est le comportement actuel du * bug *?

Décrivez ce qui se passe.

### Quel est le comportement * correct * attendu?

Décrivez ce que vous devriez voir à la place. 

### Journaux et / ou captures d'écran pertinents

Collez tous les journaux pertinents - veuillez utiliser des blocs de code (`` '') pour formater la sortie de la console, les journaux et le code
 car il est difficile de lire autrement. 

### Corrections possibles

Si vous le pouvez, créez un lien vers la ligne de code qui pourrait être responsable du problème. 

/ label ~Bug
