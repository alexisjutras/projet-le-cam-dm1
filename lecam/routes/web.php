<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DossiersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware'=>'auth'],function (){

        Route::get('/', 'FichierController@connexion');

        Route::get('/dossiers', 'DossiersController@index')->name('Dossier');
        Route::post('/dossiers/{id}/archiver', 'DossiersController@archiver');
        Route::post('/dossiers/{id}/desarchiver', 'DossiersController@desarchiver');

    Route::group(['middleware'=>'Secretaire'],function (){
        Route::get('/comptes', 'ComptesController@index');
        Route::get('/comptes/creer','ComptesController@create');
        Route::post('/comptes/creer','ComptesController@store');
        Route::get('/comptes/{id}/modifier','ComptesController@edit');
        Route::post('/comptes/{id}/modifier','ComptesController@update');
        Route::post('/comptes/{id}/supprimer', 'ComptesController@destroy');
        Route::get('/comptes/{id}/consulter','ComptesController@show');
        Route::post('/comptes/{id}/consulter','ComptesController@show');
    });

    Route::group(['middleware'=>'Employe'],function (){
        Route::get('/notes', 'NotesController@index');
        Route::get('/notes/creer', 'NotesController@create');
        Route::post('/notes/creer', 'NotesController@store');
        Route::post('/notes/{id}/supprimer','NotesController@destroy');
        Route::get('/notes/{id}/afficher','NotesController@show');
        Route::post('/notes/{id}/afficher','NotesController@show');

        Route::get('/rencontres/{id}', 'RencontresController@index');
        Route::post('/rencontres/{id}/supprimer','RencontresController@destroy');
        Route::get('/rencontres/{id}/creer','RencontresController@create');
        Route::post('/rencontres/{id}/creer','RencontresController@store');

        Route::get('/agenda', 'AgendaController@index');
        Route::get('/fichier', 'FichierController@index');
        Route::get('/fichier/{id}/edit','FichierController@edit');
        Route::post('/fichier/{id}/edit','FichierController@update');
        Route::post('/fichier/{id}/supprimer','FichierController@destroy');


        Route::post('/fichier/{id}/etatpriver', 'FichierController@etatpriver');
        Route::post('/fichier/{id}/etatpublic', 'FichierController@etatpublic');

        Route::get('/fichier/{id}/creer', 'FichierController@create');
        Route::post('/fichier/{id}/creer', 'FichierController@store');

        Route::post('/fichier/{id}', 'FichierController@create');
    });

    Route::get('/fichier/{id}', 'FichierController@show');
    Route::get('/fichier/{id}/download', 'FichierController@download');
});
