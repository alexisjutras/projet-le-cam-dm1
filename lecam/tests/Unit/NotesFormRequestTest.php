<?php

namespace Tests\Unit;

use App\Http\Requests\NotesFormRequest;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\User;
class NotesFormRequestTest extends TestCase
{
    use RefreshDatabase;


    /**
     * A basic test example.
     *
     * @return void
     */

    /**
     * @dataProvider provider
     */

    public function testBasicTest($nomExpediteur,$message,$date,$UtilisateurId,$valide)
    {
        $this->seed();
        $regles = (new NotesFormRequest())->rules();
        $data = ['nomExpediteur'=>$nomExpediteur,'message'=>$message,'date'=>$date,'UtilisateurId'=>$UtilisateurId];
        $validator = app()->get('validator');

        $this->assertEquals($valide,
            $validator
                ->make($data, $regles)
                ->passes());
    }
    public function provider() {
        return [
            'test normal'                   => ["Admin Test",
                                                "Mon message est Bonjour",
                                                "2020-12-25",
                                                1, true],

            'test message vide'             => ["Admin Test",
                                                str_repeat('x',0),
                                                "2020-12-25",
                                                1, false],
            'test message à 255 caractères' => ["Admin Test",
                                                str_repeat('x',255),
                                                "2020-12-25",
                                                1, true],

            'test message à 256 caractères' => ["Admin Test",
                                                str_repeat('x',256),
                                                "2020-12-25",
                                                1, false],

            'test nom expediteur vide'      => [str_repeat('x',0),
                                                "Bonjour",
                                                "2020-12-25",
                                                1, false],

            'test date vide'                => ["Admin Test",
                                                "Bonjour",
                                                str_repeat('x',0),
                                                1, false],

            'test date invalide'            => ["Admin Test",
                                                "Bonjour",
                                                str_repeat('x',8),
                                                1, false],

            'UtilisateurID vide'           => ["Admin Test",
                                                "Mon message est Bonjour",
                                                "2020-12-25",
                                                "", false],

            'UtilisateurID non valide'     => ["Admin Test",
                                                "Mon message est Bonjour",
                                                "2020-12-25",
                                                "eagaefaed", false],

        ];
    }
}
