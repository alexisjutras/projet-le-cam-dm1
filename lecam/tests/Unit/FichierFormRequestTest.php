<?php

namespace Tests\Feature;

use App\Http\Requests\FichierFormRequest;
use Faker\Provider\File;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\User;
use Illuminate\Http\Testing\FileFactory;

class FichierFormRequestTest extends TestCase
{
    use RefreshDatabase;


    /**
     * A basic test example.
     *
     * @return void
     */

    /**
     * @dataProvider provider
     */

    public function testBasicTest($nom,$typeFichierId,$dossierId,$file,$valide)
    {
        $this->seed();
        $regles = (new FichierFormRequest())->rules();
        $data = ['nom'=>$nom,'typeFichierId'=>$typeFichierId,'dossierId'=>$dossierId,'file'=>$file];
        $validator = app()->get('validator');

        $this->assertEquals($valide,
            $validator
                ->make($data, $regles)
                ->passes());
    }
    public function provider()
    {
        $fichier = new FileFactory();
        $fichi = $fichier->create("fichier.pdf", 10);

        /*
         * Test pour Resolve "Problème de l'ajout d'un fichier qui n'est pas obligatoire" issues 93
         */

        return [
            'test normal'               => ["je suis un nom de fichier",
                                            1,
                                            1,
                                            $fichier->create("fichier.pdf", 10),
                                            true],
            'test nom vide'             => ["",
                                            1,
                                            1,
                                            $fichier->create("fichier.pdf", 10),
                                            false],
            'test nom trop long'        => [str_repeat('x',300),
                                            1,
                                            1,
                                            $fichier->create("fichier.pdf", 10),
                                            false],
            'test type non existan'     => ["je suis un nom de fichier",
                                            15555,
                                            1,
                                            $fichier->create("fichier.pdf", 10),
                                            false],

            'test dossierId non existan' => ["je suis un nom de fichier",
                                            1,
                                            1444,
                                            $fichier->create("fichier.pdf", 10),
                                            false],
            'test nom fichier vide'     => ["je suis un nom de fichier",
                                            1,
                                            1,
                                            $fichier->create("", 10),
                                            false],
            'test taile fichier vide'   => ["je suis un nom de fichier",
                                            1,
                                            1,
                                            $fichier->create("je suis un nom de fichier", 0),
                                            false],




        ];
    }



}
