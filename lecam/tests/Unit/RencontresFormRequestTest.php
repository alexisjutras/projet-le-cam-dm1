<?php

namespace Tests\Unit;

use App\Http\Requests\RencontresFormRequest;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\User;

class RencontresFormRequestTest extends TestCase
{
    use RefreshDatabase;


    /**
     * A basic test example.
     *
     * @return void
     */

    /**
     * @dataProvider provider
     */

    public function testBasicTestRencontre($date,$description,$dossierId,$typeRencontreId,$valide)
    {
        $this->seed();
        $regles = (new RencontresFormRequest())->rules();
        $data = [ 'date'=>$date,'description' => $description,'dossierId' =>$dossierId,'typeRencontreId' =>$typeRencontreId,];
        $validator = app()->get('validator');

        /*
        if ($numero == 202006333){
            $v = $validator->make($data,$regles );
            dd($v->errors());
        }
        */


        $this->assertEquals($valide,
            $validator
                ->make($data, $regles)
                ->passes());

    }

    public function provider()
    {
        return [
            'test normal' =>            ["2020-10-14",
                                        "description",
                                        "1",
                                        "1",
                                        true],

            'date vide' =>              ["",
                                        "description",
                                        "1",
                                        "1",
                                        false],

            'date fause' =>              ["436436b d",
                                        "description",
                                        "1",
                                        "1",
                                        false],

            'description vide' =>       ["2020-10-14",
                                        "",
                                        "1",
                                        "1",
                                        false],

            'description trop grande' =>["2020-10-14",
                                        str_repeat('a',256),
                                        "1",
                                        "1",
                                        false],
            'description limit max' =>  ["2020-10-14",
                                        str_repeat('a',255),
                                        "1",
                                        "1",
                                        true],
            'description limit min' =>  ["2020-10-14",
                                        str_repeat('a',4),
                                        "1",
                                        "1",
                                        true],
            'description trop petit' => ["2020-10-14",
                                        str_repeat('a',3),
                                        "1",
                                        "1",
                                        false],

            'dossier id vide' =>        ["2020-10-14",
                                        "description",
                                        "",
                                        "1",
                                        false],
            'dossier id non valide' =>  ["2020-10-14",
                                        "description",
                                        "kkjk",
                                        "1",
                                        false],
            'typeRencontreId vide' =>   ["2020-10-14",
                                        "description",
                                        "1",
                                        "",
                                        false],
            'typeRencontreId non valide' =>["2020-10-14",
                                        "description",
                                        "1",
                                        "vgjh",
                                        false],


        ];
    }


}
