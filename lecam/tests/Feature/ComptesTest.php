<?php

namespace Tests\Feature;


use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\URL;
use Tests\TestCase;

class ComptesTest extends TestCase
{
    use RefreshDatabase;



    /*
     * Test CRUD compte avec bonne données
     */

    //Fonction qui test la création d'un utilisateur
    public function  test_créer_un_compte_avec_de_bonnes_données(){

        $this->seed();

        $user = User::where('TypeUtilisateurId', '2')->first();
        $this->actingAs($user);

        $compte = [
           'numero'=>"202102123",
           'lastname'=>"Doe",
           'firstname'=>"John",
           'email'=>"john.doe@gmail.com",
           'password'=> password_hash('John',PASSWORD_DEFAULT),
           'phone'=>"8194776599",
           'dateBirth'=>"1998-06-22",
           'TypeUtilisateurId'=>"4",
           'civique'=>"103",
           'rue'=>"Boulevard des cerises",
           'ville'=>"Drummondville",
        ];

        $reponse = $this->post('/comptes/creer',$compte);
        $reponse->assertRedirect('/comptes');


    }

    //Fonction qui test suppression un utilisateur
    public function test_effacer_un_compte_qui_est_dans_la_bd(){
        $this->seed();
        $compte = User::all()->last();
        $reponse = $this->post('/comptes/'.$compte->id.'/supprimer');
        $this->assertDatabaseMissing('users',['id'=>'$compte->id']);

    }

    //Fonction qui test la modification d'un utilisateur
    public function test_modifier_lastname_de_compte_avec_bonnes_données_et_redirige_a_index(){
        $this->seed();
        $user = User::where('TypeUtilisateurId', '2')->first();
        $this->actingAs($user);
        $compte = User::all()->last();
        $compte->lastname = $compte->lastname.'Test';
        $reponse = $this->post('/comptes/'.$compte->id.'/modifier',  [
            'numero'=>$compte->numero,
            'lastname'=>$compte->lastname,
            'firstname'=>$compte->firstname,
            'email'=>$compte->email,
            'password'=>$compte->password,
            'phone'=>$compte->phone,
            'address'=>$compte->address,
            'dateBirth'=>$compte->dateBirth,
            'TypeUtilisateurId'=>$compte->TypeUtilisateurId,
        ]);

        $compteModifie = User::find($compte->id);
        $this->assertEquals($compte->lastname,$compteModifie->lastname);
        $reponse->assertRedirect('/comptes');

    }

    public function test_index_compte()
    {
        $response = $this->get('/comptes');

        $response->assertStatus(302);
    }
    public function test_create_compte()
    {
        $response = $this->get('/comptes/creer');

        $response->assertStatus(302);
    }
    public function test_modifier_compte()
    {
        $response = $this->get('/comptes/{id}/modifier');

        $response->assertStatus(302);
    }
    public function test_consulter_compte()
    {
        $response = $this->get('comptes/{id}/consulter');

        $response->assertStatus(302);
    }
    public function test_supprimer_compte()
    {
        $response = $this->get('/comptes/{id}/supprimer');

        $response->assertStatus(405);
    }
}
