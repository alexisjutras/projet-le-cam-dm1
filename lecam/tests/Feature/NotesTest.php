<?php

namespace Tests\Feature;

use App\Note;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NotesTest extends TestCase
{
    use RefreshDatabase;

    /*
     * Test note avec bonne données
     */

    //Fonction qui test la création d'une note

    public function test_creer_note_avec_bonne_données(){
        $this->seed();
        $user = User::where('TypeUtilisateurId', '3')->first();
        $this->actingAs($user);

        $note = [
            'message'=>'Bonjour Michelle !',
            'messageVu'=>'false',
            'nomExpediteur'=>$user->firstname.' '.$user->lastname,
            'date'=>'2021-02-25',
            'UtilisateurId'=>'2',
        ];
        $reponse = $this->post('/notes/creer',$note);
        $reponse->assertRedirect('/notes');
    }

    //Fonction qui test la suppression d'une note
    public function test_effacer_une_note_qui_est_dans_la_bd(){
        $this->seed();
        $note = Note::all()->last();
        $reponse = $this->post('/note/'.$note->id.'/supprimer');
        $this->assertDatabaseMissing('note',['id'=>'$note->id']);
    }



    public function test_index_notes()
    {
        $response = $this->get('/notes');

        $response->assertStatus(302);
    }

    public function test_creer_notes()
    {
        $response = $this->get('/notes/creer');

        $response->assertStatus(302);
    }
    public function test_afficher_notes()
    {
        $response = $this->get('notes/{id}/afficher');

        $response->assertStatus(302);
    }
    public function test_supprimer_notes()
    {
        $response = $this->get('notes/{id}/supprimer');

        $response->assertStatus(405);
    }

}
