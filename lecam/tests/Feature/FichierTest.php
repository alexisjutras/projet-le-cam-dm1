<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FichierTest extends TestCase
{

    public function test_index_fichier()
    {
        $response = $this->get('/fichier');
        $response->assertStatus(302);
    }

    public function test_create_fichier()
    {
        $response = $this->get('/fichier/{id}/creer');
        $response->assertStatus(302);
    }
    public function test_modifier_fichier()
    {
        $response = $this->get('/fichier/{id}/edit');
        $response->assertStatus(302);
    }

    public function test_supprimer_fichier()
    {
        $response = $this->get('/fichier/{id}/supprimer');
        $response->assertStatus(405);
    }

}
