<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DossierTest extends TestCase
{
    use RefreshDatabase;


    /*
    * Test des fonctions de dossier
    */

    //Fonction qui test l'archivage de dossier
    public function test_archivage(){
        $this->seed();
        $user = User::where('TypeUtilisateurId', '3')->first();
        $this->actingAs($user);

        $client = User::where('TypeUtilisateurId', '4')->first();
        $reponse = $this->post('/dossiers/'.$client->id.'/archiver');
        $reponse->assertRedirect('/dossiers');
    }

    //Fonction qui test le désarchivage de dossier
    public function test_desarchivage(){
        $this->seed();
        $user = User::where('TypeUtilisateurId', '3')->first();
        $this->actingAs($user);

        $client = User::where('TypeUtilisateurId', '4')->first();
        $reponse = $this->post('/dossiers/'.$client->id.'/desarchiver');
        $reponse->assertRedirect('/dossiers');
    }

    //Fonction qui test l'index de dossier
    public function test_index_dossier()
    {
        $response = $this->get('/dossiers');
        $response->assertStatus(302);
    }

}
