<?php

namespace Tests\Feature;

use App\Rencontre;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RencontresTest extends TestCase
{
    use RefreshDatabase;
    /*
     * Test note avec bonne données
     */

    //Fonction qui test la création d'une rencontres
    public function  test_créer_une_rencontres_avec_de_bonnes_données(){
        $this->seed();
        $user = User::where('TypeUtilisateurId', '3')->first();
        $this->actingAs($user);

        $rencontre = [
            'date'=>'2021-02-25',
            'description'=>'Il a parlé de son passé.',
            'dossierId'=>'4',
            'typeRencontreId'=>'2',
        ];

        $reponse = $this->from( '/rencontres/4/creer')->post('/rencontres/4/creer',$rencontre);
        $reponse->assertRedirect('/rencontres/4/creer');

    }

    //Fonction qui test la suppression d'une rencontres
    public function test_effacer_une_rencontres_qui_est_dans_la_bd(){
        $this->seed();
        $rencontre = Rencontre::all()->last();
        $reponse = $this->post('/rencontres/'.$rencontre->id.'/supprimer');
        $this->assertDatabaseMissing('users',['id'=>'$rencontre->id']);
    }

    public function test_index_rencontres()
    {
        $response = $this->get('/rencontres/{id}');

        $response->assertStatus(302);
    }

    public function test_creer_rencontres()
    {
        $response = $this->get('/rencontres/{id}/creer');

        $response->assertStatus(302);
    }

    public function test_supprimer_rencontres()
    {
        $response = $this->get('rencontres/{id}/supprimer');

        $response->assertStatus(405);
    }
}
