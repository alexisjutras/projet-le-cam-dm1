<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDossierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Dossier', function (Blueprint $table) {
            $table->increments('id');
            $table->date('dateDeCreation');
            $table->date('dateDeModification');
            $table->boolean('etat');
            $table->bigInteger('UtilisateurId')->unsigned();
            $table->timestamps();

            $table->foreign('UtilisateurId')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dossier');
    }
}
