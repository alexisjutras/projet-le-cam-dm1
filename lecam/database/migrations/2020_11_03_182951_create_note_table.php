<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Note', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomExpediteur', 255);
            $table->boolean('messageVu');
            $table->text('message');
            $table->date('date');
            $table->bigInteger('UtilisateurId')->unsigned();
            $table->timestamps();

            $table->foreign('UtilisateurId')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note');
    }
}
