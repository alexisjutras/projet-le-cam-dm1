<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Cle', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom', 100);
            $table->string('code', 100);
            $table->integer('groupeEmployerClientId')->unsigned();
            $table->timestamps();

            $table->foreign('groupeEmployerClientId')->references('id')->on('groupeEmployerClient');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cle');
    }
}
