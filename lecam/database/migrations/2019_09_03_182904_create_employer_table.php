<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Employer', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('utilisateurId')->unsigned();
            $table->integer('groupeEmployerClientId')->unsigned();
            $table->timestamps();

            $table->foreign('utilisateurId')->references('id')->on('users');
            $table->foreign('groupeEmployerClientId')->references('id')->on('groupeEmployerClient');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer');
    }
}
