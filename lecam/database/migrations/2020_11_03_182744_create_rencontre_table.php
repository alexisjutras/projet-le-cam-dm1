<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRencontreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Rencontre', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->date('date');
            $table->integer('dossierId')->unsigned();
            $table->integer('typeRencontreId')->unsigned();
            $table->timestamps();

            $table->foreign('dossierId')->references('id')->on('dossier')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('typeRencontreId')->references('id')->on('TypeRencontre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rencontre');
    }
}
