<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichierTable extends Migration
{
    /**
     * Run the migrations.
     *
     *
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Fichier', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom', 255);
            $table->text('path');
            $table->text('extension');
            $table->text('original_name');
            $table->date('dateDeCreation');
            $table->date('dateDeModification');
            $table->integer('taille')->nullable();
            $table->boolean('etat'); //Si l'état est true, le fichier est public et si l'état est false, le fichier est privé.
            $table->integer('dossierId')->unsigned();
            $table->integer('typeFichierId')->unsigned();
            $table->timestamps();

            $table->foreign('dossierId')->references('id')->on('dossier')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('typeFichierId')->references('id')->on('TypeFichier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fichier');
    }
}
