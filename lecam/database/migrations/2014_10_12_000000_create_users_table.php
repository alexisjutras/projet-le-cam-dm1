<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero',9);
            $table->string('lastname',255);
            $table->string('firstname',255);
            $table->string('email',255)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password',255);
            $table->string('phone',10);
            $table->string('address',255);
            $table->date('dateBirth');
            $table->integer('TypeUtilisateurId')->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('TypeUtilisateurId')->references('id')->on('TypeUtilisateur')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
