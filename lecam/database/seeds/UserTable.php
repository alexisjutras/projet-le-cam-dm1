<?php

use App\User;
use App\TypeUtilisateur;
use Illuminate\Database\Seeder;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('Users')->delete();

        $a = TypeUtilisateur::where('nom', 'Administrateur')->first();
        $s = TypeUtilisateur::where('nom', 'Secretaire')->first();
        $e = TypeUtilisateur::where('nom', 'Employé')->first();
        $c = TypeUtilisateur::where('nom', 'Client')->first();

        $User = new User();
        $User->numero = "202012345";
        $User->lastname = "Test";
        $User->firstname = "Admin";
        $User->email = "admin@gmail.com";
        $User->password = password_hash('admin',PASSWORD_DEFAULT);
        $User->phone = "1234567891";
        $User->address = "1234 rue drummondville admin";
        $User->dateBirth = "2020-11-09";
        $User->typeUtilisateur()->associate($a);
        $User->save();

        $User = new User();
        $User->numero = "202012678";
        $User->lastname = "Test";
        $User->firstname = "Secrétaire";
        $User->email = "secretaire@gmail.com";
        $User->password = password_hash('secretaire',PASSWORD_DEFAULT);
        $User->phone = "0124567891";
        $User->address = "617 rue des ormes";
        $User->dateBirth = "2020-11-09";
        $User->typeUtilisateur()->associate($s);
        $User->save();

        $User = new User();
        $User->numero = "202012890";
        $User->lastname = "Test";
        $User->firstname = "Employer";
        $User->email = "employer@gmail.com";
        $User->password = password_hash('employer',PASSWORD_DEFAULT);
        $User->phone = "8194329876";
        $User->address = "1 Sylvan";
        $User->dateBirth = "2020-11-09";
        $User->typeUtilisateur()->associate($e);
        $User->save();

        $User = new User();
        $User->numero = "202012123";
        $User->lastname = "Test";
        $User->firstname = "Client";
        $User->email = "client@gmail.com";
        $User->password = password_hash('client',PASSWORD_DEFAULT);
        $User->phone = "0980980987";
        $User->address = "14 Rue des lilas";
        $User->dateBirth = "2020-11-09";
        $User->typeUtilisateur()->associate($c);
        $User->save();
    }
}
