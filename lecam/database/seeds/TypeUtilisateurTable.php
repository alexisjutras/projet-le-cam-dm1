<?php

use App\TypeUtilisateur;
use Illuminate\Database\Seeder;

class TypeUtilisateurTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TypeUtilisateur')->delete();

        $TypeUtilisateur = new TypeUtilisateur();
        $TypeUtilisateur->nom = "Administrateur";
        $TypeUtilisateur->description = "L'administrateur est la personne qui a accès à tout.";
        $TypeUtilisateur->save();

        $TypeUtilisateur = new TypeUtilisateur();
        $TypeUtilisateur->nom = "Secretaire";
        $TypeUtilisateur->description = "La Secrétaire est une employée qui a plus d'accès qu'un employé.";
        $TypeUtilisateur->save();

        $TypeUtilisateur = new TypeUtilisateur();
        $TypeUtilisateur->nom = "Employé";
        $TypeUtilisateur->description = "L'employé est une personne qui travaille dans l'entreprise et qui a accès aux dossiers de ses clients.";
        $TypeUtilisateur->save();

        $TypeUtilisateur = new TypeUtilisateur();
        $TypeUtilisateur->nom = "Client";
        $TypeUtilisateur->description = "Le client est un utilisateur qui a accès seulement à certains des documents à son sujet.";
        $TypeUtilisateur->save();
    }
}
