<?php

use App\TypeFichier;
use Illuminate\Database\Seeder;

class TypeFichierTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TypeFichier')->delete();

        $TypeFichier = new TypeFichier();
        $TypeFichier->nom = "PDF";
        $TypeFichier->save();

        $TypeFichier = new TypeFichier();
        $TypeFichier->nom = "Texte";
        $TypeFichier->save();

        $TypeFichier = new TypeFichier();
        $TypeFichier->nom = "Word";
        $TypeFichier->save();

        $TypeFichier = new TypeFichier();
        $TypeFichier->nom = "Autre";
        $TypeFichier->save();
    }
}
