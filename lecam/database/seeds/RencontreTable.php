<?php

use App\Rencontre;
use App\TypeRencontre;
use App\Dossier;
use Illuminate\Database\Seeder;

class RencontreTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Rencontre')->delete();

        $a = Dossier::where('dateDeCreation', '2020-12-05')->first();
        $b = TypeRencontre::where('nom', 'Bureau')->first();

        $Rencontre = new Rencontre();
        $Rencontre->date = "2020-11-09";
        $Rencontre->description = "description description description";
        $Rencontre->dossiers()->associate($a);
        $Rencontre->typeRencontres()->associate($b);
        $Rencontre->save();

    }
}
