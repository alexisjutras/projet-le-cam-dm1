<?php

use App\Dossier;
use App\User;
use Illuminate\Database\Seeder;

class DossierTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Dossier')->delete();

        $a = User::where('firstname', 'Admin')->first();
        $s = User::where('firstname', 'Secrétaire')->first();
        $e = User::where('firstname', 'Employer')->first();
        $c = User::where('firstname', 'Client')->first();

        $Dossier = new Dossier();
        $Dossier->dateDeCreation = "2020-12-08";
        $Dossier->dateDeModification = "2020-12-10";
        $Dossier->etat = "0";
        $Dossier->comptes()->associate($a);
        $Dossier->save();

        $Dossier = new Dossier();
        $Dossier->dateDeCreation = "2020-12-08";
        $Dossier->dateDeModification = "2020-12-10";
        $Dossier->etat = "0";
        $Dossier->comptes()->associate($s);
        $Dossier->save();

        $Dossier = new Dossier();
        $Dossier->dateDeCreation = "2020-12-08";
        $Dossier->dateDeModification = "2020-12-10";
        $Dossier->etat = "0";
        $Dossier->comptes()->associate($e);
        $Dossier->save();

        $Dossier = new Dossier();
        $Dossier->dateDeCreation = "2020-12-05";
        $Dossier->dateDeModification = "2020-12-10";
        $Dossier->etat = "0";
        $Dossier->comptes()->associate($c);
        $Dossier->save();
    }
}
