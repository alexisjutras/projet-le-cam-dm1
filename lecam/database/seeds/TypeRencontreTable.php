<?php

use App\TypeRencontre;
use Illuminate\Database\Seeder;

class TypeRencontreTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TypeRencontre')->delete();

        $TypeRencontre = new TypeRencontre();
        $TypeRencontre->nom = "Bureau";
        $TypeRencontre->save();

        $TypeRencontre = new TypeRencontre();
        $TypeRencontre->nom = "Zoom";
        $TypeRencontre->save();

        $TypeRencontre = new TypeRencontre();
        $TypeRencontre->nom = "Téléphone";
        $TypeRencontre->save();

        $TypeRencontre = new TypeRencontre();
        $TypeRencontre->nom = "Autres";
        $TypeRencontre->save();
    }
}
