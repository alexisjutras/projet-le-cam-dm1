<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TypeFichierTable::class,
            TypeUtilisateurTable::class,
            TypeRencontreTable::class,
            UserTable::class,
            DossierTable::class,
            RencontreTable::class,
            NoteTable::class,
        ]);
    }
}
