<?php

use App\Note;
use App\User;
use Illuminate\Database\Seeder;

class NoteTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Note')->delete();

        $a = User::where('firstname', 'Admin')->first();
        $s = User::where('firstname', 'Secrétaire')->first();
        $e = User::where('firstname', 'Employer')->first();

        $Note = new Note();
        $Note->message = "message 1";
        $Note->messageVu = false;
        $Note->nomExpediteur = "Admin Test";
        $Note->date = "2020-12-01";
        $Note->comptes()->associate($a);
        $Note->save();

        $Note = new Note();
        $Note->message = "message 2";
        $Note->messageVu = false;
        $Note->nomExpediteur = "Admin Test";
        $Note->date = "2020-12-01";
        $Note->comptes()->associate($s);
        $Note->save();

        $Note = new Note();
        $Note->message = "message 3";
        $Note->messageVu = false;
        $Note->nomExpediteur = "Admin Test";
        $Note->date = "2020-12-01";
        $Note->comptes()->associate($e);
        $Note->save();
    }
}
