<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class TypeUtilisateur extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'TypeUtilisateur';

    protected $fillable = [
        'nom', 'description'
    ];

}
