<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Dossier extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'dossier';

    protected $fillable = [
        'dateDeCreation', 'dateDeModification', 'etat', 'UtilisateurId'
    ];



    public function comptes()
    {
        return $this->belongsTo(User::class, 'UtilisateurId');
    }

}
