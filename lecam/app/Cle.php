<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Cle extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'cle';

    protected $fillable = [
        'nom', 'code', 'groupeEmployerClientId'
    ];

    public function groupeEmployerClients()
    {
        return $this->belongsTo(GroupeEmployerClient::class, 'groupeEmployerClientId');
    }
}
