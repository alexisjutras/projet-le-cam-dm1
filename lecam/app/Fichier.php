<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Fichier extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'fichier';

    protected $fillable = [
        'nom','path','original_name', 'dateDeCreation', 'dateDeModification', 'taille', 'etat', 'dossierId', 'typeFichierId','extension'
    ];

    public function dossiers()
    {
        return $this->belongsTo(Dossier::class, 'dossierId');
    }
    public function typeFichiers()
    {
        return $this->belongsTo(TypeFichier::class, 'typeFichierId');
    }
}
