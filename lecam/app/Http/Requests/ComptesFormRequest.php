<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComptesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $today = date("Ymd");
        return [
            'lastname' => 'required|max:255',
            'firstname' => 'required|max:255',
            'email' => 'required|email',
            'password' => 'required|between:8,100',
            'phone' => 'required|digits:10',
            'dateBirth' => 'required|date|before_or_equal:'.$today,
            'TypeUtilisateurId' => 'required|exists:App\TypeUtilisateur,id',
            'civique' => 'required|max:10',
            'rue' => 'required|max:120',
            'ville' =>'required|max:120'

        ];
    }
}
