<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RencontresFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'=>'required|date',
            'description'=>'required|between:4,255',
            'dossierId'=>'required|exists:App\Dossier,id',
            'typeRencontreId'=>'required|exists:App\TypeRencontre,id',
        ];
    }
}
