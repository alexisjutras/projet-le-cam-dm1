<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComptesModifyFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $today = date("Ymd");
        return [
            'lastname' => 'required|max:255',
            'firstname' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'dateBirth' => 'required|date|before_or_equal:'.$today,
            'TypeUtilisateurId' => 'required|exists:App\TypeUtilisateur,id',
            'address' => 'required|max:255'
        ];
    }
}
