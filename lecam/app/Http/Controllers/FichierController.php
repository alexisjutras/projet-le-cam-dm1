<?php

namespace App\Http\Controllers;

use App\Dossier;


use App\User;
use Illuminate\Http\Request;
use App\Fichier;
use App\TypeFichier;
use App\TypeUtilisateur;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\File;
use App\Http\Requests\FichierFormRequest;


class FichierController extends Controller
{
    /**
     * ((Lister les fichiers (REQ-9-6))
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $id = Auth::user()->id;
        $role = Auth::user()->TypeUtilisateurId;

        dd($role);

        $fichiers = Fichier::where('dossierId', $id)->get();
        $dossier = Dossier::where('UtilisateurId', $id)->first();

        return view('fichier.index', compact('fichiers','dossier','role'));
    }

    /**
     * Redirige vers la page d'ajout d'un fichier.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $typeFichier = TypeFichier::all();

        $dossier = Dossier::where('id', $id)->first();
        return view('fichier.create', compact('dossier','typeFichier'));
    }

    /**
     * ((Téléverser un fichier (REQ-5-5))
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(FichierFormRequest $request)
    {
        $a = date("Y-m-d");

       if($request->hasFile('fichier')){


            $file = $request->file('fichier');

            $file->getSize();
            $pathe = $request->file('fichier')->store('fichier');

            $fichier = new Fichier(array(
                'nom' => $request->get('nom'),
                'dateDeCreation' => $a,
                'dateDeModification' => $a,
                'dossierId' => $request->get('dossierId'),
                'taille' => $file->getSize(),
                'etat' => "0",
                'typeFichierId' => $request->get('typeFichierId'),
                'path' => $pathe,
                'original_name' => $file->getClientOriginalName(),
                'extension' => $file->getClientOriginalExtension(),


            ));
            $fichier->save();

            return redirect()->back()->with('status', 'Fichier a été ajouté avec succès');
        }

        return redirect()->back();

    }

    /**
     * ((Consulter un dossier (REQ-9-12))
     *
     * @param  int  $id
     * @return
     */
    public function show($id)
    {
        $fichiers = Fichier::where('dossierId', $id)->get();
        $dossier = Dossier::where('id', $id)->first();
        $r = Auth::user()->TypeUtilisateurId;
        $role = TypeUtilisateur::where('id', $r)->first();

        return view('fichier.index', compact('fichiers','dossier','role'));
    }

    /**
     * Ne fonctionne pas pour le moment
     *
     * @param  int  $id
     * @return
     */
    public function edit($id)
    {
        $dossier = Dossier::where('id', $id)->get();
        $TypeFichier = TypeFichier::all();
        return view('fichier.modifier', compact('dossier','TypeFichier'));
    }

    /**
     * N'est pas utilisé pour le moment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return
     */
    public function update($id, FichierFormRequest $request)
    {

        $a = date("Y-m-d");

        $fichier = Fichier::findOrFail($id);
        $fichier->nom = $request->get('nom');
        $fichier->dateDeModification = $a;
        $fichier->typeFichierId = $request->get('typeFichierId');

        $fichier->save();
        return redirect()->back()->with('status', 'Fichier a été ajouté avec succès');

    }

    /**
     * ((Supprimer un fichier (REQ-9-10))
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fichier = Fichier::findOrFail($id);
        Storage::delete($fichier->path);
        $fichier->delete();

        return redirect()->back()->with('status', 'Le fichier a été effacé.');

    }

    /**
     * Page qui s'affiche après la connexion.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function connexion()
    {
        $id = Auth::user()->id;
        $fichiers = Fichier::where('dossierId', $id)->get();
        $dossier = Dossier::where('id', $id)->first();

        return view('fichier.index', compact('fichiers','dossier'));
    }

    /**
     * ((Télécharger une fichier (REQ-5-4))
     *
     * @param  int  $f_id
     */

    public function download($f_id)
    {
        $fichier = Fichier::findOrFail($f_id);

        return response()->download(storage_path("/app/$fichier->path"),"$fichier->nom.$fichier->extension");

    }

    public function etatpriver($id)
    {
        $fichiers = Fichier::findOrFail($id);
        $fichiers->etat = 1;

        $fichiers->save();
        return redirect()->back();

    }

    public function etatpublic($id)
    {
        $fichiers = Fichier::findOrFail($id);
        $fichiers->etat = 0;

        $fichiers->save();
        return redirect()->back();

    }
}
