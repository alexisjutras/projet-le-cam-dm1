<?php


namespace App\Http\Controllers;

use App\Http\Requests\RencontresFormRequest;
use App\Rencontre;
use App\TypeRencontre;
use App\Dossier;
use Illuminate\Http\Request;

class RencontresController extends Controller
{
    /**
     * ((Lister les rencontres (REQ-39-1))
     *
     * @param int $id
     * @return
     */
    public function index($id)
    {
        $rencontres = Rencontre::where('dossierId', $id)->get();
        $dossiers = Dossier::where('id', $id)->get();
        return view('rencontres.index', compact('rencontres','dossiers'));
    }

    /**
     * Redirige vers la page pour créer une rencontre.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $rencontres = Rencontre::where('id', $id)->get();
        $dossiers = Dossier::where('id', $id)->get();
        $TypeRencontre = TypeRencontre::all();
        return view('rencontres.creer', compact('rencontres','dossiers','TypeRencontre'));
    }

    /**
     * ((Créer une rencontre (REQ-39-2))
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(RencontresFormRequest $request)
    {
        $rencontres = new Rencontre(array(
            'date' => $request->get('date'),
            'description' => $request->get('description'),
            'dossierId' => $request->get('dossierId'),
            'typeRencontreId' => $request->get('typeRencontreId'),
        ));

        $rencontres->save();

        return redirect()->back()->with('status', 'Votre intervention a été ajouté avec succès!');
    }

    /**
     * ((Supprimer une rencontre (REQ-39-4))
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $rencontre = Rencontre::findOrFail($id);
        $rencontre->delete();
        return redirect()->back()->with('status', 'Cette intervention a été effacé.');
    }
}
