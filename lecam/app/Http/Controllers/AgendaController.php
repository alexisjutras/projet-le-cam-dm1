<?php


namespace App\Http\Controllers;


class AgendaController extends Controller
{
    /**
     * Affiche la page de base.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('agenda.index');
    }
}
