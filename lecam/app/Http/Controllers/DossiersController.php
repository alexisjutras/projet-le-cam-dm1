<?php

namespace App\Http\Controllers;

use App\Fichier;
use App\Http\Controllers\Controller;
use App\Http\Requests\DossiersFormRequest;
use Illuminate\Http\Request;
use App\Dossier;

class DossiersController extends Controller
{
    /**
     * (Lister les dossiers (REQ-9-9))
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dossiers = Dossier::all();
        return view('dossier.index', compact('dossiers'));
    }

    /**
     * (Archiver un dossier (REQ-5-2))
     *
     * @param  int  $id
     * @return
     */
    public function archiver($id)
    {
        $dossiers = Dossier::findOrFail($id);
        $dossiers->etat = 1;

        $dossiers->save();
        return redirect('/dossiers')->with('status', 'Archivage réussi.');

    }

    /**
     * (Restaurer un dossier (REQ-10-1))
     *
     * @param  int  $id
     * @return
     */
    public function desarchiver($id)
    {
        $dossiers = Dossier::findOrFail($id);
        $dossiers->etat = 0;

        $dossiers->save();
        return redirect('/dossiers')->with('status', 'Désarchivage réussi.');

    }

}
