<?php

namespace App\Http\Controllers;

use App\Http\Requests\ComptesModifyFormRequest;
use App\Dossier;
use App\Http\Requests\ComptesFormRequest;
use Illuminate\Http\Request;
use App\User;
use App\TypeUtilisateur;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ComptesController extends Controller
{
    /**
     * (Lister les comptes (REQ-8-8))
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();
        return view('comptes.index', compact('users'));
    }

    /**
     * Redirige vers la page créer un compte.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        if (Auth::check()) {
            $role = Auth::user()->typeUtilisateur->nom;
            if ($role == 'Administrateur') {
                $TypeUtilisateurs = TypeUtilisateur::all();
            }
            if ($role == 'Secretaire') {

                $TUtilisateurs = TypeUtilisateur::all();
                $TypeUtilisateurs = $TUtilisateurs->whereIn('nom',['Employé','Client']);
            }
        }
        return view('comptes.creer', compact('TypeUtilisateurs'));
    }

    /**
     * (Créer un compte (REQ-8-1))
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(ComptesFormRequest $request)
    {
        $t= TypeUtilisateur::find($request->get('TypeUtilisateurId'));
        $users = new User(array(
            'numero' => $this->numero(),
            'lastname' => $request->get('lastname'),
            'firstname' => $request->get('firstname'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'phone' => $request->get('phone'),
            'address' =>  $this->adresse($request),
            'dateBirth' => $request->get('dateBirth'),

        ));
        $users->typeUtilisateur()->associate($t);
        $users->save();

        $a = date("Y-m-d");

        $dossier = new Dossier(array(
            'dateDeCreation' => $a,
            'dateDeModification' => $a,
            'etat' => "0",
            'UtilisateurId' => $users->id,
        ));

        $dossier->save();

        return redirect('/comptes')->with('status', 'Le compte a été enregistré avec succès!');
    }

    /**
     * (Consulter un compte (REQ-8-10))
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::findOrFail($id);

        return view('comptes.consulter', compact('users'));
    }

    /**
     * Redirige vers la page en envoyant l'utilisateur sélectionné.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        if (Auth::check()) {
            $role = Auth::user()->typeUtilisateur->nom;
            if ($role == 'Administrateur') {
                $typeUtilisateurs = TypeUtilisateur::all();
            }
            if ($role == 'Secretaire') {

                $TUtilisateurs = TypeUtilisateur::all();
                $typeUtilisateurs = $TUtilisateurs->whereIn('nom',['Employé','Client']);
            }
        }
        return view('comptes.modifier', compact('user','typeUtilisateurs'));
    }

    /**
     * (Modifier tous les comptes (REQ-8-11))
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update($id, ComptesModifyFormRequest $request)
    {
        $user = User::findOrFail($id);
        $user->numero = $request->get('numero');
        $user->lastname = $request->get('lastname');
        $user->firstname = $request->get('firstname');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->address = $request->get('address');
        $user->dateBirth = $request->get('dateBirth');
        $user->TypeUtilisateurId = $request->get('TypeUtilisateurId');

        $user->save();
        return redirect('/comptes')->with('status', 'Modification réussie.');

    }

    /**
     * (Supprimer un compte (REQ-8-4))
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/comptes')->with('status', 'Le compte a été effacé.');

    }

    /**
     * Fonction qui permet de créer l'addresse d'un compte.
     *
     * @param  int  $id
     * @return
     */
    private function adresse(ComptesFormRequest $request){
        $civique = $request->get('civique');
        $rue = $request->get('rue');
        $ville = $request->get('ville');

        return $civique." ".$rue.", ".$ville;
    }

    /**
     * Focntion qui permet de créer le numéro d'un compte.
     *
     * @param  int  $id
     * @return
     */
    private function numero(){
        $today = date("Ym");
        $nombre1 = (string)rand(0,9);
        $nombre2 = (string)rand(0,9);
        $nombre3 = (string)rand(0,9);

        return $today.$nombre1.$nombre2.$nombre3;
    }
}
