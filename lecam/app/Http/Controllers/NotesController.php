<?php

namespace App\Http\Controllers;


use App\Note;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\NotesFormRequest;

class NotesController extends Controller
{
    /**
     * (Liste les notes (REQ-12-5))
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $notes = Note::all();
        return view('notes.index', compact('notes'));
    }

    /**
     * Redirige vers la page pour créer une note.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $users = User::all();
        return view("notes.creer", compact('users'));

    }

    /**
     * (Créer une note (REQ-12-1))
     *
     */
    public function store(NotesFormRequest $request)
    {
        $notes = new Note(array(
            'nomExpediteur' => $request->get('nomExpediteur'),
            'messageVu' => false,
            'message' => $request->get('message'),
            'date' => $request->get('date'),
            'UtilisateurId' => $request->get('UtilisateurId'),
        ));

        $notes->save();

        return redirect('/notes')->with('status', 'La note a été envoyé avec succès!');
    }

    /**
     * (Consulter une note (REQ-12-7))
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $notes = Note::findOrFail($id);
        $notes->messageVu = 1;
        $notes->save();
        return view('notes.afficher', compact('notes'));
    }

    /**
     * (Supprimer une note (REQ-12-4))
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $notes = Note::findOrFail($id);
        $notes->delete();
        return redirect('/notes')->with('status', 'La note a été effacé');
    }

}
