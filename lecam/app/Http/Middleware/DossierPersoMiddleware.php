<?php

namespace App\Http\Middleware;

use App\Dossier;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class DossierPersoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dossiers = Dossier::all();


        if(Auth::user() && $request->user()->id == $dossiers->UtilisateurId ){
            return $next($request);
        }
        return new Response(view('AccesRefuse')->with('role', 'CLIENT'));

    }
}
