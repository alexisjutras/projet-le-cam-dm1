<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EmployeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $role = $request->user()->typeUtilisateur->nom;
            if($role == 'Administrateur' || $role == 'Secretaire' || $role == 'Employé'){
                return $next($request);
            }
            else
                return redirect()->back();
        }
    }
}
