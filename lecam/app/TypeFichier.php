<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeFichier extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'TypeFichier';

    protected $fillable = [
        'nom'
    ];
}
