<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class GroupeEmployerClient extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'groupeEmployerClient';

    protected $fillable = [
         'clientId'
    ];


    public function clients()
    {
        return $this->belongsTo(Client::class, 'clientId');
    }
}
