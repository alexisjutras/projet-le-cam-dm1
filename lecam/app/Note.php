<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'note';

    protected $fillable = [
        'message', 'messageVu', 'nomExpediteur', 'date', 'UtilisateurId'
    ];

    public function comptes()
    {
        return $this->belongsTo(User::class, 'UtilisateurId');
    }
}
