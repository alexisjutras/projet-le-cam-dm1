<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rencontre extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'rencontre';

    protected $fillable = [
        'date', 'description', 'dossierId', 'typeRencontreId'
    ];

    public function dossiers()
    {
        return $this->belongsTo(Dossier::class, 'dossierId');
    }

    public function typeRencontres()
    {
        return $this->belongsTo(TypeRencontre::class, 'typeRencontreId');
    }

}
