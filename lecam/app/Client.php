<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'client';

    protected $fillable = [
        'utilisateurId'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'utilisateurId');
    }
}
