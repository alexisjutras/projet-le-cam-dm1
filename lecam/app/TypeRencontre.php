<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class TypeRencontre extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'TypeRencontre';

    protected $fillable = [
        'nom'
    ];
}
