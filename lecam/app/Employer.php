<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    protected $table = 'employer';

    protected $fillable = [
        'utilisateurId', 'groupeEmployerClientId'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'utilisateurId');
    }
    public function groupeEmployerClient()
    {
        return $this->belongsTo(GroupeEmployerClient::class, 'groupeEmployerClientId');
    }
}
