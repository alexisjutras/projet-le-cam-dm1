<html>
<head>
    <title> @yield('title') </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <style>
        html,body{
            background-image: radial-gradient(#FCF304 25%,#E18611 40%, #C92023 80%);
            background-size: cover;
            background-repeat: no-repeat;
            height: auto;
        }
        .card{
            margin-top: 20%;
            margin-bottom: auto;
        }
        .card-header{
            background: #555555;
            color: white;
            text-align: center;
            font-size: large;
        }

        .btn-secondary{
            background: #555555;
            font-size: large;
        }
        .btn-link{
            color: #555555;
        }
    </style>
</head>
<body>

@yield('content')

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
