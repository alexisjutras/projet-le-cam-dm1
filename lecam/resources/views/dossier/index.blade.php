@extends('master')
@section('title', 'Dossiers')

@section('content')
    <script type="text/javascript" src="{{asset('js/search.js')}}"></script>
    <input type="text" class="form-control" onkeyup="mySearch()" placeholder="Recherche.." id="recherche" name="recherche" autocomplete=off>
    <div class="container col-md-12 col-md-offset-2 mt-3">
        <div class="card">
            <div class="card-header ">
                <h5 class="float-left">Dossiers</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                @if ($dossiers->isEmpty())
                    <p> Il n'y a pas de dossiers.</p>
                @else
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Numéro du dossiers</th>
                            <th>Nom du client</th>
                            <th>Date de création</th>
                            <th>Date de modification</th>
                            <th>État</th>
                            <th> <input type="checkbox" value="1" onclick="trier()" name="cocher" id="cocher">archive</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($dossiers as $dossiers)
                            <tr data-searchvalue=" {{$dossiers->numero}} {{$dossiers->dateDeCreation}} {{$dossiers->dateDeModification}} {{ $dossiers->comptes->firstname }} {{ $dossiers->comptes->lastname }}
                                " class="search_div" data-etat="{{$dossiers->etat}}">
                                <td>
                                    <a href="{{ action('FichierController@show', $dossiers->id) }}"> {{ $dossiers->comptes->numero }} </a>
                                </td>
                                <td> {{ $dossiers->comptes->firstname }} {{ $dossiers->comptes->lastname }} </td>
                                <td>{{ $dossiers->dateDeCreation }}</td>
                                <td>{{ $dossiers->dateDeModification }}</td>
                                @if ($dossiers->etat == 0)
                                    <td>Ouvert</td>
                                @else
                                    <td>Archivé</td>
                                @endif
                                <td>
                                    @if ($dossiers->etat == 0)
                                        <form  method="post" action="{{ action('DossiersController@archiver', $dossiers->id) }}" class="float-left">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div>
                                                <button type="submit" class="btn btn-warning">Archiver</button>
                                            </div>
                                        </form>
                                    @else
                                        <form  method="post" action="{{ action('DossiersController@desarchiver', $dossiers->id) }}" class="float-left">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div>
                                                <button type="submit" class="btn btn-info">Désarchiver</button>
                                            </div>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
