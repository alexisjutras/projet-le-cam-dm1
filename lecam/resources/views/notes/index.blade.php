@extends('master')
@section('title', 'Notes')

@section('content')
    <script type="text/javascript" src="{{asset('js/search.js')}}"></script>

    <input type="text" class="form-control" onkeyup="mySearch()" placeholder="Recherche.." id="recherche" name="recherche" autocomplete=off>
    <div class="container col-md-12 col-md-offset-2 mt-3">
        <div class="card">
            <div class="card-header ">
                <h5 class="float-left">Notes</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                @if ($notes->isEmpty())
                    <p> Il n'y a pas de notes.</p>
                @else
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Expéditeur</th>
                            <th>État</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($notes->reverse() as $note)
                            @if($note->UtilisateurId == Auth::user()->id)
                                <tr data-searchvalue="{{ $note->date }} {{ $note->nomExpediteur }}"
                                    class="search_div">
                                    <td>{{ $note->date }} </td>
                                    <td>{{ $note->nomExpediteur }} </td>

                                    @if($note->messageVu == 0)
                                        <td>Message non lu <span class="badge badge-success">!</span></td>
                                    @else
                                        <td>Message lu</td>
                                    @endif
                                    <td>
                                        <form  method="post" action="{{ action('NotesController@show', $note->id)}}" class="float-left">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div>
                                                <button type="submit" class="btn btn-info">Message </button>
                                            </div>
                                        </form>
                                    </td>
                                    <td>
                                        <form  method="post" action="{{ action('NotesController@destroy', $note->id) }}" class="float-left">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div>
                                                <button type="submit" class="btn btn-danger">Supprimer</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
