@extends('master')
@section('title', 'Notes')

@section('content')
    <div class="container col-md-12 col-md-offset-2 mt-3">
        <div class="card">
            <div class="card-header ">
                <h5 class="float-left">Message</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                <table class="table">
                    <thead>
                    <td>De : {{ $notes->nomExpediteur }}</td>

                    </thead>
                    <tbody>
                    <td>{{ $notes->message }} </td>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </tbody>
                </table>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <a href="/notes" class="btn btn-info">Retour</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
