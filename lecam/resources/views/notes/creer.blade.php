@extends('master')
@section('title', 'Créer une note')

@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">Envoyer une note</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                <form method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset>
                        <div class="form-group">
                            <label for="date" class="col-lg-12 control-label">Date : </label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control" id="date" name="date" readonly value="{{ now()->toDateString()}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nomExpediteur" class="col-lg-12 control-label">Expéditeur : </label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control" id="nomExpediteur" name="nomExpediteur" readonly value="{{ Auth::user()->firstname }} {{Auth::user()->lastname}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="UtilisateurId" class="col-lg-12 control-label">Envoyer à : </label>
                            <div class="col-lg-12">
                                <select class="select-user" id="UtilisateurId" name="UtilisateurId">

                                    @foreach ($users as $users)
                                        @if ($users->TypeUtilisateurId != 4 )
                                            <option value="{{$users->id}}">{{$users->firstname}} {{$users->lastname}}</option>

                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-lg-12 control-label">Message : </label>
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="3" id="message" name="message"></textarea>
                                @error('message')
                                <label style="color: red;">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <a href="/notes" class="btn btn-danger">Annuler</a>
                                <button type="submit" class="btn btn-primary">Envoyer</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection
