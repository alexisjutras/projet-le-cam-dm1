@extends('master')
@section('title', 'Fichiers')

@section('content')

    <script type="text/javascript" src="{{asset('js/search.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ url('/css/custom.css') }}" />
    <input type="text" class="form-control" onkeyup="mySearch()" placeholder="Recherche.." id="recherche" name="recherche" autocomplete=off>
    <div class="container col-md-12 col-md-offset-2 mt-3">
        <div class="card">
            <div class="card-header ">
                <div class="align-center">
                    <h5 class="float-left">Dossier personnel {{$dossier->comptes->firstname}} {{$dossier->comptes->lastname}}  </h5>
                </div>
                <?php $role = Auth::user()->typeUtilisateur->nom;?>

                <div class="clearfix"></div>
                @if($dossier->comptes->typeUtilisateur->nom == 'Client' && $role != 'Client')
                <a href="/rencontres/{{$dossier->comptes->id}}"><button  type="submit" class="btn btn-primary">Afficher les interventions</button></a>
                @endif
            </div>

            <div>
                <div align="center">
                    @if($role != 'Client')
                    <a href="/fichier/{{$dossier->id}}/creer"> <button type="submit" class="btn btn-primary btn-circle">+</button></a>
                    <label class="border-primary"> Ajouter un fichier</label>
                    @endif
                </div>
            </div>

            <div class="card-body mt-2">
                @if($fichiers->isEmpty())
                    <p>Il n'y a pas de fichier dans le dossiers.</p>
                @else
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @elseif(session('error'))
                        <div class="alert alert-success">
                            {{ session('error') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Date de création</th>
                            <th>Date de modification</th>
                            <th>Type du fichier</th>
                            @if($role != 'Client')
                            <th>État</th>
                            @else
                                <th></th>
                            @endif
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fichiers as $fichier)
                            @if (($role == 'Client' && $fichier->etat == 1) || ($role != 'Client'))
                                <tr data-searchvalue="{{$fichier->nom}} {{$fichier->dateDeCreation}} {{$fichier->dateDeModification}} {{ $fichier->taille }} {{ $fichier->typeFichiers->nom }}
                                    " class="search_div">
                                    <td>{{ $fichier->nom}}</td>
                                    <td>{{ $fichier->dateDeCreation }}</td>
                                    <td>{{ $fichier->dateDeModification }}</td>
                                    <td>{{ $fichier->typeFichiers->nom }} </td>
                                    <td>
                                        @if ($fichier->etat == 0)
                                            <form  method="post" action="{{ action('FichierController@etatpriver', $fichier->id) }}" class="float-left">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div>
                                                    <button type="submit" class="btn btn-warning">Privé</button>
                                                </div>
                                            </form>
                                        @else
                                            <form  method="post" action="{{ action('FichierController@etatpublic', $fichier->id) }}" class="float-left">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div>
                                                    @if($role != 'Client')
                                                    <button type="submit" class="btn btn-info">Public</button>
                                                    @endif
                                                </div>
                                            </form>
                                        @endif
                                    </td>
                                    <td>
                                        <form  method="post" action="{{ action('FichierController@destroy', $fichier->id) }}" class="float-left">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div>
                                                @if($role != 'Client')
                                                <button type="submit" class="btn btn-danger">Supprimer</button>
                                                @endif
                                            </div>
                                        </form>
                                    </td>
                                    <td>
                                        <form  method="get" action="{{ action('FichierController@download', $fichier->id ) }}" class="float-left">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div>
                                                <button type="submit" class="btn btn-info">Télécharger</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                @endif
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection

