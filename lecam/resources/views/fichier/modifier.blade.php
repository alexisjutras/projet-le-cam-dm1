@extends('master')
@section('title', 'Ajouter un fichier')

@section('content')

    <div class="container col-md-8 col-md-offset-2">

        <div>
            <div class="card mt-5">
                <h5 class="float-left">Ajouter un fichier à l'utilisateur {{$dossiers[0]->comptes->firstname}} {{$dossiers[0]->comptes->lastname}}</h5>
                <div class="clearfix"></div>
            </div>

            <form method="post">
                @foreach ($errors->all() as $error)
                    <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <label for="nom" >Nom du dosier :</label>
                    <input type="text" id="nom" name="nom">
                </div>

                <div class="form-group">
                    <label for="TypeFichier" class="col-lg-12 control-label">Type de fichier</label>
                    <div class="col-lg-12">
                        <select class="select-TypeFichier" id="TypeFichier" name="typeFichierId">

                            @foreach ($TypeFichier as $TypeFichier)

                                <option value="{{$TypeFichier->id}}">{{$TypeFichier->nom}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <a href="/fichier/{{$dossiers[0]->id}}" class="btn btn-default">Quitter</a>
                    <button type="submit" class="btn btn-info">Ajouter</button>
                </div>
            </form>
        </div>
    </div>








@endsection

