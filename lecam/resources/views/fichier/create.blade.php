@extends('master')
@section('title', 'Ajouter un fichier')

@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header">
                <h5 class="float-left">Ajouter un fichier à l'utilisateur {{$dossier->comptes->firstname}} {{$dossier->comptes->lastname}}</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                <form method="post" enctype="multipart/form-data">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <fieldset>
                            <div class="form-group">
                                <label for="nom" class="col-lg-12 control-label">Nom du fichier:</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" id="nom" name="nom">
                                    @error('nom')
                                    <label style="color: red;">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                <input id="fichier" name="fichier" class="file" type="file" multiple data-preview-file-type="any">
                                    @error('fichier')
                                    <label style="color: red;">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="TypeFichier" class="col-lg-12 control-label">Type de fichier:</label>
                                <div class="col-lg-12">
                                    <select class="select-TypeFichier" id="TypeFichier" name="typeFichierId">

                                        @foreach ($typeFichier as $TypeFichier)
                                            <option value="{{$TypeFichier->id}}">{{$TypeFichier->nom}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dossierId" class="col-lg-12 control-label">Numéro de dossiers:</label>
                                <div class="col-lg-12">

                                    <input type="text" class="form-control" readonly value="{{$dossier->comptes->numero}}">
                                    <input type="hidden" class="form-control" id="dossierId" name="dossierId" readonly value="{{$dossier->id}}">

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <a href="/fichier/{{$dossier->id}}" class="btn btn-default">Quitter</a>
                                    <button type="submit" class="btn btn-info">Ajouter</button>
                                </div>
                            </div>
                        </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection

