@extends('master')
@section('title', 'Créer un compte')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div  class="card">
                    <div class="card-header" align="center" >
                        <h5  align="center">{{ __('Création de compte') }}</h5>
                        <div class="clearfix"></div></div>
                    <div class="card-body">
                        <form method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>
                                <div class="form-group row">
                                    <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Prénom') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Prénom">
                                        @error('firstname')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Nom') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Nom">
                                        @error('lastname')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right" >{{ __('Courriel') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="exemple@gmail.com" >
                                        @error('email')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" id="password" name="password" require autocomplete="new-password">
                                        @error('password')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Numéro de téléphone') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="phone" name="phone" >
                                        @error('phone')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="civique" class="col-md-4 col-form-label text-md-right ">{{ __('Numéro Civique') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="civique" name="civique" >
                                        @error('civique')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="rue" class="col-md-4 col-form-label text-md-right ">{{ __('Rue') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="rue" name="rue" >
                                        @error('rue')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ville" class="col-md-4 col-form-label text-md-right ">{{ __('Municipalité') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="ville" name="ville" >
                                        @error('ville')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="dateBirth" class="col-md-4 col-form-label text-md-right">{{ __('Date de naissance') }}</label>
                                    <div class="col-md-6">
                                        <input type="date" class="form-control" id="dateBirth" name="dateBirth" >
                                        @error('dateBirth')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row" >
                                    <label for="TypeUtilisateur" class="col-md-4 col-form-label text-md-right">{{ __("Type d'utilisateur") }}</label>
                                    <div class="col-md-6 ">
                                        <select class="select-TypeUtilisateur" id="TypeUtilisateur" name="TypeUtilisateurId">

                                            @foreach ($TypeUtilisateurs->reverse() as $TypeUtilisateur)

                                                <option value="{{$TypeUtilisateur->id}}">{{$TypeUtilisateur->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <a href="/comptes" class="btn btn-default">{{ __('Annuler') }}</a>
                                        <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                </div>
        </div>
    </div>
@endsection
