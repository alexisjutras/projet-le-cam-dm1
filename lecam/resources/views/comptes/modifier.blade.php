@extends('master')
@section('title', 'Modifier un client')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header ">
                        <h5  align="center">{{ __('Modifier un compte') }}</h5>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card-body mt-2">
                        <form method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>
                                <div class="form-group">
                                    <label for="numero" class="col-lg-12 control-label">Numéro</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" id="numero" name="numero" readonly value="{{ $user->numero}}">
                                        @error('numero')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-lg-12 control-label">Nom</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $user->lastname }}">
                                        @error('lastname')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="firstname" class="col-lg-12 control-label">Prénom</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $user->firstname }}">
                                        @error('firstname')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-12 control-label">Courriel</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}">
                                        @error('email')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-lg-12 control-label">Téléphone</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                                        @error('phone')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-lg-12 control-label">Adresse</label>
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" id="address" name="address" value="{{ $user->address }}">
                                        @error('address')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="dateBirth" class="col-lg-12 control-label">Date de naissance</label>
                                    <div class="col-lg-12">
                                        <input type="date" class="form-control" id="dateBirth" name="dateBirth" value="{{ $user->dateBirth }}">
                                        @error('dateBirth')
                                        <label style="color: red;">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="TypeUtilisateurId" class="col-lg-12 control-label">Type d'utilisateur</label>
                                    <div class="col-lg-12">
                                        <select class="select-TypeUtilisateur" id="TypeUtilisateurId" name="TypeUtilisateurId" value="{{ $user->TypeUtilisateurId }}">

                                            @foreach ($typeUtilisateurs as $typeUtilisateurs)
                                                <option value="{{$typeUtilisateurs->id}}">{{$typeUtilisateurs->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <a href="/comptes" class="btn btn-primary">Annuler</a>
                                        <button type="submit" class="btn btn-primary">Modifier</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
