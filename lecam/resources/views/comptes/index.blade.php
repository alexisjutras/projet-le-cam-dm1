@extends('master')
@section('title', 'Comptes')

@section('content')
    <script type="text/javascript" src="{{asset('js/search.js')}}"></script>

    <input type="text" class="form-control" onkeyup="mySearch()" placeholder="Recherche.." id="recherche" name="recherche" autocomplete=off>
    <div class="container col-md-12 col-md-offset-2 mt-3">
        <div class="card">
            <div class="card-header " align="center">
                <h5>Comptes</h5>
                <a href="/comptes/creer"><button  type="submit" class="btn btn-primary">Ajouter</button></a>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                @if ($users->isEmpty())
                    <p> Il n'y a pas de compte.</p>
                @else
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Numéro</th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                            <th>Adresse</th>
                            <th>Date de naissance</th>
                            <th>Type d'utilisateur</th>
                            <th></th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr data-searchvalue="{{ $user->id }} {{ $user->firstname }} {{ $user->lastname }} {{ $user->email }}
                            {{ $user->phone }} {{ $user->address }} {{ $user->dateBirth }} {{ $user->typeUtilisateur->nom }}"
                                class="search_div">

                                <td><a href="{{ action('ComptesController@edit', $user->id) }}">{{ $user->numero }} </a> </td>
                                <td>{{ $user->lastname }}</td>
                                <td>{{ $user->firstname}}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->address }}</td>
                                <td>{{ $user->dateBirth }}</td>
                                <td>{{ $user->typeUtilisateur->nom }}</td>
                                <td>
                                    <form  method="post" action="{{ action('ComptesController@show', $user->id)}}" class="float-left">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div>
                                            <button type="submit" class="btn btn-info">Consulter</button>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <form  method="post" action="{{ action('ComptesController@destroy', $user->id) }}" class="float-left">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div>
                                            <button type="submit" class="btn btn-danger">Supprimer</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
