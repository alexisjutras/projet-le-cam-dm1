@extends('master')
@section('title', 'Consulter un compte')

@section('content')
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" align="center" >
                        <h5>{{ __('Informations du compte') }}</h5>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            <fieldset>
                                <div class="form-group row">
                                    <label for="numero" class="col-md-4 col-form-label text-md-right">{{ __('Numéro :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="numero" name="numero" readonly value="{{ $users->numero }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Nom :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="lastname" name="lastname" readonly value="{{ $users->lastname }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Prénom :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="firstname" name="firstname" readonly value="{{ $users->firstname }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Courriel :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="email" name="email" readonly value="{{ $users->email }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Numéro de téléphone :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="phone" name="phone" readonly value="{{ $users->phone }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Adresse :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="address" name="address" readonly value="{{ $users->address }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="dateBirth" class="col-md-4 col-form-label text-md-right">{{ __('Date de naissance :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="dateBirth" name="dateBirth" readonly value="{{ $users->dateBirth }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="UtilisateurId" class="col-md-4 col-form-label text-md-right">{{ __('Type :') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="UtilisateurId" name="UtilisateurId" readonly value="{{ $users->typeUtilisateur->nom }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <a href="/comptes" class="btn btn-info">Retour</a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
