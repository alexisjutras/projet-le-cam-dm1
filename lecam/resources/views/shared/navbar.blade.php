<nav class="navbar navbar-expand-lg  navbar-light " >

    <!-- Left Side Of Navbar -->
    <a href="https://lecam.ca/">
        <img src="{{URL::asset("/img/CamLogo.svg")}}"  width="120">
    </a>


    <div class="container">

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Middle Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>


            <ul class="navbar-nav mr-auto h6" >
                <li class="nav-item {{(request()->segment(1)=='dossierPersonnel')?'active':''}} ">
                    <a class="nav-link " href="/fichier/{{Auth::user()->id}}">Votre Dossier</a>
                </li>
                <?php $role = Auth::user()->typeUtilisateur->nom;?>
                @if($role != 'Client')
                    <li class="nav-item {{(request()->segment(1)=='dossiers')?'active':''}}">
                        <a class="nav-link" href="/dossiers">Dossiers</a>
                    </li>

                    <?php $bool = false;
                    $notes = App\Note::all()?>
                    @foreach($notes as $note)
                        @if($note->UtilisateurId == Auth::user()->id)
                            @if($note->messageVu == 0)
                                <?php $bool = true;?>
                            @endif
                        @endif
                    @endforeach
                    @if($bool == true)
                        <li class="nav-item {{(request()->segment(1)=='notes')?'active':''}}">
                            <a class="nav-link" href="/notes">Notes <span class="badge badge-success">!</span></a>
                        </li>
                    @else
                        <li class="nav-item {{(request()->segment(1)=='notes')?'active':''}}">
                            <a class="nav-link" href="/notes">Notes</a>
                        </li>
                    @endif

                    <li class="nav-item {{(request()->segment(1)=='agenda')?'active':''}}">
                        <a class="nav-link" href="/agenda">Agenda</a>
                    </li>
                    @if($role == 'Administrateur' || $role == 'Secretaire')
                        <li class="nav-item {{(request()->segment(1)=='comptes')?'active':''}}">
                            <a class="nav-link" href="/comptes">Comptes</a>
                        </li>
                    @endif
                @endif
                </ul>



            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle h6" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->firstname }} {{Auth::user()->lastname}} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right  " aria-labelledby="navbarDropdown" >
                            <a class="dropdown-item font-weight-bold" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Déconnexion') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>

            </ul>

        </div>

    </div>

</nav>

