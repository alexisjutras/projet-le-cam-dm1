<link rel="stylesheet" type="text/css" href="{{ url('/css/boutonNote.css') }}" />
<?php $role = Auth::user()->TypeUtilisateurId;?>
@if($role != 4)
<div class = "bottomright">
        <a href="/notes/creer"><button  type="submit" class="btn btn-success btn-circle">Envoyer une note</button></a>
</div>
    @endif
