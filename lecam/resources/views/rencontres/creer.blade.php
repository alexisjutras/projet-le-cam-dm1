@extends('master')
@section('title', 'Ajouter une intervention')

@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="card mt-5">
            <div class="card-header">
                <h5 class="float-left">Ajouter une intervention à {{$dossiers[0]->comptes->firstname}} {{$dossiers[0]->comptes->lastname}}</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                <form method="post">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset>
                        <div class="form-group">
                            <label for="date" class="col-lg-12 control-label">Date</label>
                            <div class="col-lg-12">
                                <input type="date" class="form-control" id="date" name="date">
                                @error('date')
                                <label style="color: red;">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-lg-12 control-label">Description</label>
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="3" id="description" name="description" placeholder="Description"></textarea>
                                @error('description')
                                <label style="color: red;">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dossierId" class="col-lg-12 control-label">Numéro de dossiers</label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control" readonly value="{{$dossiers[0]->comptes->numero}}">
                                <input type="hidden" class="form-control" id="dossierId" name="dossierId" readonly value="{{$dossiers[0]->comptes->id}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="TypeRencontre" class="col-lg-12 control-label">Type de l'intervention</label>
                            <div class="col-lg-12">
                                <select class="select-TypeRencontre" id="TypeRencontre" name="typeRencontreId">
                                    @foreach ($TypeRencontre as $TypeRencontre)
                                        <option value="{{$TypeRencontre->id}}">{{$TypeRencontre->nom}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <a href="/rencontres/{{$dossiers[0]->id}}" class="btn btn-default">Quitter</a>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection

