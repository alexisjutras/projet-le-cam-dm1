@extends('master')
@section('title', 'Rencontres')

@section('content')
    <script type="text/javascript" src="{{asset('js/search.js')}}"></script>

    <input type="text" class="form-control" onkeyup="mySearch()" placeholder="Recherche.." id="recherche" name="recherche" autocomplete=off>
    <div class="container col-md-12 col-md-offset-2 mt-3">
        <div class="card">
            <div class="card-header ">
                <h5 class="float-left">Interventions {{$dossiers[0]->comptes->firstname}} {{$dossiers[0]->comptes->lastname}}</h5>
                <div class="clearfix"></div>
                <a href="/rencontres/{{$dossiers[0]->id}}/creer"><button  type="submit" class="btn btn-primary">Ajouter une intervention</button></a>
                <a href="/fichier/{{$dossiers[0]->id}}" class="btn btn-primary">Retourner au dossiers</a>
            </div>

            <div class="card-body mt-2">
                @if ($rencontres->isEmpty())
                    <p> Il n'y a pas d'intervention.</p>
                @else
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Type de l'intervention</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rencontres as $rencontre)
                            <tr data-searchvalue="{{ $rencontre->date }} {{ $rencontre->typeRencontres->nom }}"
                                class="search_div">
                                <td>{{ $rencontre->date }} </td>
                                <td>{{ $rencontre->description }} </td>
                                <td>{{ $rencontre->typeRencontres->nom }} </td>
                                <td>
                                    <form  method="post" action="{{ action('RencontresController@destroy', $rencontre->id) }}" class="float-left">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div>
                                            <button type="submit" class="btn btn-danger">Supprimer</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
